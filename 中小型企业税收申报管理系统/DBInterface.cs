﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 中小型企业税收申报管理系统
{
    public interface DBInterface
    {
        /// <summary>
		/// 开始一个事务
		/// </summary>
		void BeginTransaction();
        /// <summary>
        /// 提交事务
        /// </summary>
        void Commit();
        /// <summary>
        /// 回滚事务
        /// </summary>
        void RollBack();
        /// <summary>
        /// 打开一个连接
        /// </summary>
        void OpenConnection();
        /// <summary>
        /// 关闭一个连接
        /// </summary>
        void CloseConnection();
        /// <summary>
        /// 设定要执行的SQL命令的信息
        /// </summary>
        /// <param name="sqlCommand">SQL命令</param>
        /// <param name="commandType">命令类型</param>
        void SQLCommand(string sqlCommand, CommandType commandType);
        /// <summary>
        /// 添加于SQL命令响应的相关参数
        /// </summary>
        /// <param name="paraName">参数名</param>
        /// <param name="oleDbType">参数类型</param>
        /// <param name="length">参数长度</param>
        /// <param name="paraValue">参数值</param>
        /// <returns>返回值</param>
        object AddParameter(string paraName, SqlDbType sqlDbType, int length, object paraValue);
        /// <summary>
        /// 更新Bolb类型的字段值
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="blobFiled">BLOB字段名</param>
        /// <param name="key">主键</param>
        /// <param name="keyValue">主键值</param>
        /// <param name="date">数据值</param>
        /// <returns>true:成功，false:失败</returns>
        bool UpdataBlobFiled(string tableName, string blobFiled, string key, string keyValue, byte[] date, int offset);
        /// <summary>
        /// 读取BLOB类型的字段值
        /// </summary>
        /// <param name="i">BLOB字段的索引</param>
        /// <param name="stream">输出流</param>
        void ReadBlobFiled(int i, System.IO.Stream stream);
        /// <summary>
        /// 执行SQL命令与数据库交互
        /// </summary>
        /// <returns>SQL命令影响的行数</returns>
        int ExecuteNonQuery();
        /// <summary>
        /// 执行SQL查询命令，并返回查询结果
        /// </summary>
        /// <returns></returns>
        IDataReader ExecuteQuery();
        /// <summary>
        /// 执行SQL查询命令，并返回DataSet对象
        /// </summary>
        /// <returns>DataSet对象</returns>
        DataSet ExecuteDataSetQuery();
        /// <summary>
        /// 用DataSet对象，更新数据库
        /// </summary>
        /// <param name="dataset"></param>
        void UpdateDataSet(DataSet dataset);
        /// <summary>
        /// 释放相关资源
        /// </summary>
        void Release();
    }
    /// <summary>
    /// 用户接口工厂
    /// </summary>
    public class DBFactory
    {
        private DBFactory()
        { }
        public static DBInterface Instance(string className)
        {
            Type type = null;
            object obj = null;
            try
            {
                type = Type.GetType(className);
            }
            catch (TypeLoadException e)
            {
                throw e;
            }
            if (type == null)
            {
                throw new Exception("类型加载失败！");
            }
            obj = Activator.CreateInstance(type);
            if (obj is 中小型企业税收申报管理系统.DBInterface)
            {
                return (DBInterface)obj;
            }
            else
            {
                throw new Exception("此类没有实现Db.Operation.DBInterface接口");
            }
        }
    }
}
