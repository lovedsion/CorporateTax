﻿CREATE TABLE [dbo].[T001]--税收统计表
(
	[T00101] UNIQUEIDENTIFIER NOT NULL, 
    [T00102] VARCHAR(4) COLLATE Chinese_PRC_CI_AS not null,
    [T00103] VARCHAR(6) COLLATE Chinese_PRC_CI_AS not null,
    [T00104] NVARCHAR(200) COLLATE Chinese_PRC_CI_AS null,
    [T00105] NUMERIC(18, 2) null,
    [T00106] NUMERIC(18, 2) null,
    [T00107] NUMERIC(18, 2) null,
    [T00108] NUMERIC(18, 2) null,
    [T00109] NUMERIC(18, 2) null,
    [T00110] NUMERIC(18, 2) null,
    [T00111] NUMERIC(18, 2) null,

)
