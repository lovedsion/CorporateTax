﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace 中小型企业税收申报管理系统
{
    public partial class Login : Form
    {
        /// <summary>
        /// 变量定义
        /// </summary>
        private DBInterface db;
        private bool blCanLogin = false;
        private string strUser = "";            //记录用户名
        private string guid = null;               //用户GUID
        private string dataTime = null;
        private string strU00202 = null;
        private string strU00205 = null;
        private string popedom = "";

        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            doSelect();

        }

        private void doSelect()
        {
            //构建查询语句
            StringBuilder SQLQueryStr = new StringBuilder();
            SQLQueryStr.Append("select U00101,U00102,U00103,U00105,U00203,U00202,U00205 from U001,U002 where U00104=U00201 and U00102='");
            SQLQueryStr.Append(txtU00102.Text);
            SQLQueryStr.Append("' and U00103='");
            SQLQueryStr.Append(txtU00103.Text);
            SQLQueryStr.Append("'");
            //连接数据库，进行查询
            try
            {
                db.OpenConnection();
                db.SQLCommand(SQLQueryStr.ToString(), CommandType.Text);
                IDataReader read = db.ExecuteQuery();
                if (read.Read())
                {
                    this.txtU00102.Text = read["U00102"].ToString();
                    this.getU00105 = read["U00105"].ToString();
                    this.guid = read["U00101"].ToString();
                    this.popedom = read["U00203"].ToString();
                    this.strU00202 = read["U00202"].ToString();
                    this.strU00205 = read["U00205"].ToString();
                    this.blCanLogin = true;
                }
                else if (txtU00102.Text == "" && txtU00103.Text == "")
                {
                    UtilClass.message("用户名密码不能为空！");
                    txtU00102.Focus();
                }

                else
                {
                    UtilClass.message("用户名或密码错误,请重新输入!");
                    txtU00102.Text = "";
                    txtU00103.Text = "";
                    txtU00102.Focus();
                }

            }
            catch (WebException webEx)
            {
                webEx.ToString();
                UtilClass.message("网络繁忙，请稍候重试！");
            }
            catch (SqlException ex)
            {
                ex.ToString();
                UtilClass.message("网络繁忙或数据库没有启动，请稍候重试！");
            }
            catch
            {

            }
            finally
            {
                db.CloseConnection();
            }
        }
    }
}
